# Prueba TicTacToe WebSocket

El codigo está desarrollado con Node.js y Websocket de tal manera que al abrir el servidor y ver su funcionalidad sea posible abrirlo en dos navegadores en tiempo real para poderlo jugar.

¿Como activar el servidor?

`npm start`

Y abrir `index.html` en la carpeta client.

Basado en el video de [YouTube](https://www.youtube.com/watch?v=V7lZiZlSTKQ&t=118s)
